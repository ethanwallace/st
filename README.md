# Ethan's St 

This is a copy of my st configuration. It is stock, except I have added
the patches for scrollback via `SHIFT PgUp` and `SHIFT PgDn`, as well as
a patch to hide the X11 cursor when typing. The patches are applied as
follows: `patch -p1 -i [Patches/PATCH.diff]`

I have also changed to font to
[mononoki](https://madmalik.github.io/mononoki).
